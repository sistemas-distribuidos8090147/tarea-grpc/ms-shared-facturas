import express from 'express';
import factura_routes from './routes/factura.routes.js';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/factura', factura_routes);

export default app;