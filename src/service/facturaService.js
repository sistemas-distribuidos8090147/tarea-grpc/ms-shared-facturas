import Factura from '../models/factura.js';

export const saveFactura = async ( factura ) => {
    const { ciudad, empleado, cliente, codigo, fecha,
        valorTotal, valorSubTotal, valorIva, items } = factura;
    
    const facturaModel = new Factura({ ciudad, empleado, cliente, codigo, fecha,
        valorTotal, valorSubTotal, valorIva, items });
    
    const saved = await facturaModel.save();
    return saved;
}

export const getAllFacturas = async () => {
    const facturas = await Factura.find();
    return facturas;
}