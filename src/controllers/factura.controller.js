import { saveFactura, getAllFacturas } from '../service/facturaService.js';

export const createFactura = async (req, res) => {
    try {
        const saved_factura = await saveFactura(req.body);
        
        return res.status(201).json({
            _id: saved_factura._id,
            ciudad: saved_factura.ciudad,
            empleado: saved_factura.empleado,
            cliente: saved_factura.cliente,
            codigo: saved_factura.codigo,
            fecha: saved_factura.fecha,
            valorTotal: saved_factura.valorTotal,
            valorSubTotal: saved_factura.valorSubTotal,
            valorIva: saved_factura.valorIva,
            items: saved_factura.items
        });
    } catch(error) {
        console.error(error);
    }
}

export const getFacturas = async (req, res) => {
    try {
        const facturas = await getAllFacturas();
        return res.status(200).json(facturas);
    } catch(error) {
        console.error(error);
    }
}