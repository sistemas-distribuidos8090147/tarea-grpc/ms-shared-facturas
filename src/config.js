const config = {
    PORT: 3000,
    MONGO_URI: 'mongodb://localhost:27017/shared-facturas-db',
    GRPC_HOST: 'localhost',
    GRPC_PORT: 3043
}

export default config;