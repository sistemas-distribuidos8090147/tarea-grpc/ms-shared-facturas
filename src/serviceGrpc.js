import grpc from '@grpc/grpc-js';
import protoLoader from '@grpc/proto-loader';
import { saveFactura } from './service/facturaService.js';
import config from './config.js';

const PROTO_FILE = './proto/service.proto';

async function createGrpcServer(){
    const packageDefinition = protoLoader.loadSync(PROTO_FILE, {
        keepCase: true,
        longs: String,
        enums: String,
        arrays: true,
    });
    
    const facturaProto = grpc.loadPackageDefinition(packageDefinition);
    const server = new grpc.Server();
    
    server.addService(facturaProto.FacturasService.service, {
        enviarFactura: (call, callback) => {
            const factura = call.request;
            saveFactura(factura);
            callback(null, factura);
        }
    });
    
    await server.bindAsync(
        `${config.GRPC_HOST}:${config.GRPC_PORT}`,
        grpc.ServerCredentials.createInsecure(),
        () => {
            server.start();
            console.log( `gRPC server listening on port ${config.GRPC_PORT}` );
        }
    );
}
createGrpcServer();