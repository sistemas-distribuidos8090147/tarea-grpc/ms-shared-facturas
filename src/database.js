import mongoose from 'mongoose';
import config from './config.js';

mongoose.set('strictQuery', false);

mongoose.connect(config.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then((data) => console.log('DB se encuentra conectada.'))
    .catch((error) => console.log(error));