import mongoose from 'mongoose';

const facturaSchema = new mongoose.Schema({
    ciudad: { 
        type: String,
        required: true
    },
    empleado: { 
        type: String,
        required: true
    },
    cliente: { 
        type: String, 
        required: true
    },
    codigo: { 
        type: String,
        required: true
    },
    fecha: { 
        type: String,
        required: true
 },
    valorTotal: {
        type: Number,
        required: true
    },
    valorSubTotal: {
        type: Number,
        required: true
    },
    valorIva: {
        type: Number,
        required: true
    },
    items: [
        { 
            type: JSON, 
            required: true }
    ]
});
export default mongoose.model('factura', facturaSchema);