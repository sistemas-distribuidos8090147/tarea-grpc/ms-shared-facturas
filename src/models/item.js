import mongoose, { Schema } from "mongoose"

const itemSchema = new Schema({
    cantidad: Number,
    producto: String,
    valorPorProducto: Number,
    valorUnitario: Number
}, {
    versionKey: false
})

export default mongoose.model('item', itemSchema)