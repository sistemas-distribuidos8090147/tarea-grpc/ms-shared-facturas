# Indicaciones Generales
Este microservicio desarrollado en Node.js funciona con una base de datos de MongoDB y fue desarrollado con Node v18.18.0.

## Para ejecutar el microservicio
1. Ejecutar el comando `npm install`.
2. La configuración de la base de datos Mongo está en el archivo `src/config.js`.
3. Ejecutar el comando `npm start`.

## Para probar la aplicación
- El microservicio está levantado en el puerto 3000.
- Como este microservicio solo recibe datos, a continuación se muestra el endpoint para consultar los datos existentes en la base de datos:
    1. Para consultar todos los registros de facturas (GET Request): `http://localhost:3000/factura/getFacturas`.

## Para levantar una base de datos MongoDB usando Docker
- En el ambiente de desarrollo se utilizó la imagen oficial de MongoDB para levantar una instancia de base de datos y la imagen de Mongo Express como DBMS.
- Para levantar una base de datos MongoDB con Mongo Express como DBMS:
    1. Se crea la red de docker para la comunicación entre contenedores: `docker network create facturas-api`.
    2. Se crea el contenedor de la imagen de MongoDB: `docker run --network facturas-api --name mongoGRPCdb -d -p 27017:27017 mongo`.
    3. Se crea el contenedor de la imagen de Mongo Express: `docker run --network facturas-api -e ME_CONFIG_MONGODB_SERVER=mongoGRPCdb -p 9001:8081 mongo-express`.